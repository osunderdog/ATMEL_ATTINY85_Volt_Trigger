
#include "main.h" 

// -------- Global Variables --------- //

// -------- Functions --------- //

void initADC(void) {
  
  //ADC Multiplexer Selection Register
  ADMUX =
    (0 << ADLAR) |		/* using full 10 bit resolution.*/
    (0 << REFS2) | 
    (0 << REFS1) |
    (0 << REFS0) |
    (1 << MUX0) |		/* using ADC1 (PB2) */
    (0 << MUX1) |
    (0 << MUX2) |
    (0 << MUX3)
    ;

  //ADC Control and Status register A
  ADCSRA =
    (1 << ADEN) |		/* Enable ADC */
    (0 << ADSC) |		/* do not start conversion yet */
    (0 << ADATE) |		/* do not auto trigger conversion. */
    (1 << ADPS2) |		/* prescaler related to clock */
    (1 << ADPS1) |
    (0 << ADPS0)
    ;
  
}

uint16_t read16ADC() {

  ADCSRA |= (1 << ADSC);	/* start the conversion */

  //ADCSRA ADC Control and Status Register A
  //ADSC: ADC Start Conversion.
  //ADSC: Reads 1 until conversion is complete.
  //Checking bit ADSC to see if it's clear.

  loop_until_bit_is_clear(ADCSRA, ADSC);

  uint8_t adc_low = ADCL;
  uint16_t raw_adc = (ADCH << 8) | adc_low;
  
  /* Add low and high bit to get 16 bit number. */
  return raw_adc;
}

//enable pins on block B for led output.
void initLED(void) {
  LED_REG =
    (1 << G_LED) |
    (1 << Y_LED) |
    (1 << A_LED);
}

void onLEDPortB(uint8_t led_pin) {
  LED_PORT |= (1 << led_pin);
}

void offLEDPortB(uint8_t led_pin) {
  LED_PORT &= ~(1 << led_pin);
}


int main(void) {
  // -------- Inits --------- //
  uint16_t adcValue; 

  uint16_t middleValue = 0;
  uint16_t highValue = 0;
  uint16_t lowValue = 0;
  uint16_t noiseVolume = 0;

  uint8_t padding = INITIAL_PADDING;
  uint8_t noiseState = C_NOTHING;

  uint8_t warmup_delay_index = WARMUP_DELAY_COUNT;
  
  initLED();
  initADC();

  // All LEDs on to check before starting conversions.
  LED_PORT |= ((1 << A_LED) | (1<< G_LED) | (1 << Y_LED));
  _delay_ms(10);
  LED_PORT &= ~((1 << A_LED));
  _delay_ms(10);
  LED_PORT &= ~((1<< G_LED));
  _delay_ms(10);
  LED_PORT &= ~((1 << Y_LED));

  
#ifdef DEBUG
  initUSART();
  printString("OK");
#endif
  
  // ------ Event loop ------ //
  while (1) {

    //blink amber on each ADC read.
    onLEDPortB(A_LED);
    adcValue = read16ADC();
    offLEDPortB(A_LED);

    middleValue = adcValue + ROUND_SHIFT(middleValue);
    if(adcValue >= (middleValue >> 4)) {
      //reading is above middle... add it to the high value.
      highValue = adcValue + ROUND_SHIFT(highValue);
    }
    if(adcValue <= (middleValue >> 4)) {
      //reading is below the middle .. add it to the low value.
      lowValue = adcValue + ROUND_SHIFT(lowValue);
    }

    //Amplitude of the voltage signal.
    //Noise volume is unsigned (and don't let it overflow)
    if(highValue > lowValue + padding){
      noiseVolume = (highValue - lowValue) + padding;
    } else {
      noiseVolume = 0;
    }

    //Check to see if noiseVolume is above
    //--------------------------------------------------
    // HIGH REGION
    //--------------------------------------------------
    if(noiseVolume > NOISE_THRESHOLD_HIGH)
      {
	//previous state C_NOTHING..
	if(noiseState == C_HIGH)
	  {
	    //was high, is high.
	  }
	else if(noiseState == C_NOTHING)
	  {
	    //was nothing is high
	    //shouldn't get here.
	  }
	else if(noiseState == C_LOW)
	  {
	    //was low is high
	    onLEDPortB(G_LED);

	    if(warmup_delay_index == 0)
	      {
		//do something profound with DASH
	      }
	    
	  }
	//Setting noiseState to HIGH...
	noiseState = C_HIGH;
      }
    //--------------------------------------------------
    // LOW REGION
    //--------------------------------------------------    
    else if (noiseVolume > NOISE_THRESHOLD_LOW)
      {
        if (noiseState == C_HIGH)
	  {
	    //was high is low.
	    //turn off led indicating that pump is on.
	    offLEDPortB(G_LED);

	    if(warmup_delay_index == 0)
	      {
		//do something profound with DASH
	      }

	  }
	else if(noiseState == C_NOTHING)
	  {
	    //was nothing is low.
	    //turn on led PB5
	    onLEDPortB(Y_LED);
	  }
	else
	  {
	    //was low is low.
	  }
	
	noiseState = C_LOW;
      }
    //--------------------------------------------------
    // BELOW LOW REGION
    //--------------------------------------------------    
    else
      {
	if(noiseState == C_HIGH)
	  {
	    //was high is nothing
	  }
	else if (noiseState == C_NOTHING)
	  {
	    //was nothing is nothing.
	  }
	else if (noiseState == C_LOW)
	  {
	    //was low is nothing
	  }
	
	offLEDPortB(G_LED);
	offLEDPortB(Y_LED);
	noiseState = C_NOTHING;
      }

#ifdef DEBUG
    //    printString("adcValue\t");  
    printWord(adcValue);   
    printString("\t");  

    //printString("lowValue\t"); 
    printWord(lowValue);  
    printString("\t");  

    //printString("middleValue\t"); 
    printWord(middleValue); 
    printString("\t"); 
    
    //printString("highValue\t");  
    printWord(highValue);   
    printString("\t");  

    //printString("noiseVolume\t");  
    printWord(noiseVolume);   
    printString("\n");
#endif
    
    //initial action during warmup.
    if(warmup_delay_index > 0) 
      { 
    	//during warmup, go through the motions, but don't trigger servo. 
    	onLEDPortB(A_LED); 
    	_delay_ms(CYCLE_DELAY_MS); 
    	offLEDPortB(A_LED); 
    	warmup_delay_index--; 
      } 
    else 
      { 
	_delay_ms(CYCLE_DELAY_MS);
      }
  }                                                  /* End event loop */
  return (0);                            /* This line is never reached */
}
