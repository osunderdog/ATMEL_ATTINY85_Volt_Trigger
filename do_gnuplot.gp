#set terminal pdf

filename = "./data/20161120_03.ed"

# assume column definitions are:
# adcValue,lowValue,middleValue,highValue,noiseVolume

plot filename u 1 w l title "adcValue",\
'' u 2 w l title "lowValue",\
'' u 3 w l title "middleValue",\
'' u 4 w l title "highValue",\
'' u 5 w l title "noiseVolume"

pause -1