
// Standard AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>

// Standard includes
#include <stdlib.h>

// These are optional, but nice to have around.
// Feel free to comment them out if you don't use them.
#include "macros.h"

//rounding method for high low and middle values.
//NOTE if you change x-0 term, you'll need to consider the case where result can go negative
//roll over to very large number.
#define ROUND_SHIFT(x) (x - ((x - 0) >> 4))

//sensitivity.  Greater number, more noise spikes.
#define INITIAL_PADDING 0

// Used to identify a legitimate noise. trigger action above high
#define NOISE_THRESHOLD_HIGH 6000
// ignore stuff below low.
#define NOISE_THRESHOLD_LOW 500

//States for pump noise. 
#define C_NOTHING 0
#define C_LOW 1
#define C_HIGH 2



//Delay between samples.
#define CYCLE_DELAY_MS 10

//uint8_t  Max delay is 255.  if want higher will need to change approach.
//use builtin timer?
#define WARMUP_DELAY_COUNT 255

//if debug defined then uart is available
//#define DEBUG

#ifdef DEBUG
#include "USART.h"
#endif

//if servo is defined then do servo actions.
//#define SERVO

//Servo related postion and timing information.
//might need to play with this...
#define SERVO_REST 110		/* feels at rest.. right above button */
#define SERVO_PRESS 190		/* activ button press */

#define SERVO_CLICK_MS 100	/* Time to hold at press to register single click */
#define SERVO_CLICK_WAIT_MS 550	/* Time between first and second press */


//Defines to get working on ATTINY85

//Analog definitions
//pin used for analog input
//if this is changed, then need to look at DMUX definition in init.
#define CTADC0 PB5

//LED block
//DDBR Data Direction Register designate pins as in/out.
#define LED_REG DDRB

//PORT Definition Data is read or written here
#define LED_PORT PORTB

//LED indicators
#define G_LED PB0		/* Voltage is above threshold */
#define Y_LED PB1		/* Voltage is below threshold */
#define A_LED PB3		/* Indications */

